'use strict'
// Mes variables pour aller rechercher champs
const nombre_paires = document.getElementById('paires')
const nom = document.getElementById('nom')

// Les variables pour les textes ou section
const formulaire = document.getElementById('form')
const jeu = document.getElementById('jeu')
const h1 = document.getElementById('h1')
const h2 = document.getElementById('h2')
const id = document.getElementById('id')

// À propos de mes cartes
let cartesTournees = [] // Tableau qui contient cartes tournées

// Comparaison de chaîne alphanumérique
const chaineAlpha = /^[a-zA-Z0-9]+$/

formulaire.addEventListener('submit', valider)
/**
 * @param  {} {constmesPaires=document.getElementById('paires'
 * @param  {} letnombrePaires=mesPaires.valuenombrePaires=parseInt(nombrePaires
 * @param  {} constnomUser=document.getElementById('nom'
 * @param  {nombrePaires} letnomUti=nomUser.value.lengthletnomString=nomUser.valuereturn{paires
 * @param  {nomUti} nom
 * @param  {nomString}}} lettre
 * @returns nomString
 */
// Mon objet
function donnees(){
    const mesPaires = document.getElementById('paires')
    let nombrePaires = mesPaires.value
    nombrePaires = parseInt(nombrePaires)

    const nomUser = document.getElementById('nom')
    let nomUti = nomUser.value.length

    let nomString = nomUser.value
    return {paires: nombrePaires, nom: nomUti, lettre: nomString}
}
/**
 * @param  {champs_} {letchamps_='lechampdoitcontenirdescaractères'letalpha_='lechampdoitcontenirdescaractèresalphanumériquesseulement'letpaires_='vousdevezentrezdespairesentre2et10'return{champs
 * @param  {alpha_} alpha
 * @param  {paires_}}} paires
 * @returns paires_
 */
function messagesErreurs(){
    let champs_ = 'le champ doit contenir des caractères'
    let alpha_ = 'le champ doit contenir des caractères alphanumériques seulement'
    let paires_ = 'vous devez entrez des paires entre 2 et 10'

    return {champs: champs_, alpha: alpha_, paires: paires_}
}

// Mes paires à trouver + paires que j'ai trouvée
let pairesTrouvees = 0
let matchCartes = document.getElementsByClassName('match')


// Valider mon formulaire & créer le jeu 
function valider(e){

    // Le tableau des messages des erreurs
    const messages = []
    let parametres = donnees()
    let messages_erreurs = messagesErreurs()
    
    // Pour savoir s'il y a un caractères quelconque dans les input
    if (parametres.nom <= 0 || parametres.paires <= 0) {
        messages.push(messages_erreurs.champs)
    } 

    // Pour savoir si c'est une chaîne alphanumériques
    if (chaineAlpha.test(parametres.lettre) === false){
        messages.push(messages_erreurs.alpha)     
    } 

    //Pour savoir si les paires < 2 ou > 10
    if (parametres.paires < 2 || parametres.paires > 10 ) {
        messages.push(messages_erreurs.paires)
    }

    // Si y'a un message dans le tableau des messages, ne pas valider formulaire
    if (messages.length > 0) {
        e.preventDefault()
        const ul = document.getElementById('messages')
        ul.innerHTML = ""
        for(let i = 0; i < messages.length; i++){
            const li = document.createElement('li')
            const texte = document.createTextNode(messages[i])
            li.appendChild(texte)
            ul.appendChild(li)
            li.classList.add('red')
        }
    // S'il n'y a pas de messages creer jeu & générer boutons 
    }  else {
        e.preventDefault()
        cacher()
        genererBoutonEtTimer()
        ajoutText()
    }
}
/**
 * @param  {} {letsectionFormulaire=document.getElementById('section-form'
 */
// Cache la section du formulaire
function cacher(){
    let sectionFormulaire = document.getElementById('section-form')
    sectionFormulaire.style.display = 'none'
}
/**
 * @param  {trouvertouteslespairesenmoinsde5minutes'id.innerHTML='votreidentifiant:'+nom.value}} {h1.innerHTML='Lejeuestcommencé...'h2.innerHTML='Lebut
 * @returns nom
 */
// Ajoute du texte
function ajoutText(){
    h1.innerHTML = 'Le jeu est commencé...'
    h2.innerHTML = 'Le but : trouver toutes les paires en moins de 5 minutes'
    id.innerHTML = 'votre identifiant : ' + nom.value
}
/**
 * @param  {} {constnbTours=nombre_pairesconsttableauCartes=[]for(leti=0;i<nbTours.value;i++
 * @param  {} {tableauCartes.push(i
 * @param  {} tableauCartes.push(i
 */
// Mettre les cartes dans un tableau
function faireTableauCartes(){
    const nbTours = nombre_paires
    const tableauCartes = []
    for (let i = 0; i < nbTours.value; i++) {
      tableauCartes.push(i)
      tableauCartes.push(i)
    }
    return tableauCartes
}
/**
 * @param  {} {consttableauCartes=faireTableauCartes(
 * @param  {} consttableauCartesMelangees=[]while(tableauCartes.length>0
 * @param  {} {constindex=Math.floor(Math.random(
 * @param  {} *tableauCartes.length
 * @param  {} tableauCartesMelangees.push(tableauCartes[index]
 * @param  {} tableauCartes.splice(index
 * @param  {} 1
 */
// Mélanger les cartes
function tableauCartesMelangees(){
    const tableauCartes = faireTableauCartes()
    const tableauCartesMelangees = []
    while(tableauCartes.length > 0) {
        const index = Math.floor(Math.random() * tableauCartes.length)
        tableauCartesMelangees.push(tableauCartes[index])
        tableauCartes.splice(index, 1)
    }
    return tableauCartesMelangees
}
/**
 * @param  {} cartes
 * @param  {} {cartes.innerHTML=""cartes.style.height='170px'cartes.style.width='100px'jeu.appendChild(cartes
 * @param  {} cartes.classList.add('carte'
 */
// Les parametres de mes cartes à quoi elle ressemble...
function lesParametresCartes(cartes){
    cartes.innerHTML = ""
    cartes.style.height = '170px'
    cartes.style.width = '100px'
    jeu.appendChild(cartes)
    cartes.classList.add('carte')
}


// Pour générer mes cartes et afficher le timer
function genererBoutonEtTimer() {
    const intervale = demarrerTimer(5) // Mon timer
    function demarrerTimer(nombre_minutes){
        const temps = nombre_minutes * 60
        miseAJour(temps)
        const intervale = setInterval(diminuerSeconde, 1000)
        return intervale
    }
    function diminuerSeconde(){
        let nbSecondes = timer.getAttribute('data-secondes')
        nbSecondes = parseInt(nbSecondes)
        nbSecondes = nbSecondes - 1
        miseAJour(nbSecondes)

        // Arret du jeu de cartes
        if(nbSecondes === 0){
            clearInterval(intervale)
            perdu()
        }
    }
    function miseAJour(nbSecondes){
        let secondes = nbSecondes % 60
        const minutes = Math.floor(nbSecondes / 60)
        if(secondes < 10){
            secondes = "0" + secondes
        }
        const timer = document.getElementById('timer')
        timer.textContent = minutes + ":" + secondes
        timer.setAttribute('data-secondes', nbSecondes)
    }

    // Créer mes cartes
    const cartesMelangees = tableauCartesMelangees()
    jeu.innerHTML = ""
    for (let i = 0; i < cartesMelangees.length; i++) {
        const cartes = document.createElement('button')
        lesParametresCartes(cartes)
        let index = cartesMelangees[i] // Pour mettre index mélangé sur les cartes
        cartes.id = index
        cartes.setAttribute('nombre-cache', index)
        cartes.addEventListener('click', clicCartes)
    }
    return cartesMelangees
}

/**
 * @param  {} e
 * @param  {} {letnombre=donnees(
 * @param  {} cartesTournees.push(e.target.id
 * @param  {} e.target.classList.add('clic'
 * @param  {} e.target.textContent=e.target.ide.target.setAttribute('disabled'
 * @param  {} true
 * @param  {} if(cartesTournees.length===2
 * @param  {} {if(cartesTournees[0]===cartesTournees[1]
 * @param  {} {matching(
 * @param  {} pairesTrouvees=pairesTrouvees+1if(pairesTrouvees===nombre.paires
 * @param  {} {setTimeout(victoire
 * @param  {} 1000
 * @param  {} }}else{setTimeout(notSame
 * @param  {} 1000
 * @param  {} }cartesTournees.pop(
 * @param  {} cartesTournees.pop(
 */
// Lorsque je cliques sur une cartes
function clicCartes(e){
    let nombre = donnees()
    cartesTournees.push(e.target.id) // Pousse mes cartes dans cartes tournees
    e.target.classList.add('clic') // Premier clic
    e.target.textContent = e.target.id
    e.target.setAttribute('disabled', true)
    if (cartesTournees.length === 2){
        if(cartesTournees[0] === cartesTournees[1]){    
            matching()     

            pairesTrouvees  = pairesTrouvees + 1 // add les paires dans paires trouvées
            if (pairesTrouvees  === nombre.paires){
                setTimeout(victoire, 1000)
            }

        } else {
            setTimeout(notSame, 1000)
        }
        cartesTournees.pop() // Pour enlever mes cartes tournées
        cartesTournees.pop()
    }
}
/**
 * @param  {} {letmatch=document.getElementsByClassName('clic'
 * @param  {} match[0].classList.add('match'
 * @param  {} match[1].classList.add('match'
 * @param  {} match[0].classList.remove('clic'
 * @param  {} match[0].classList.remove('clic'
 */
// Lorsque j'ai un match 
function matching(){
    // J'ai un match : retourner carte & mettre disabled
    let match = document.getElementsByClassName('clic') 
    match[0].classList.add('match')
    match[1].classList.add('match')
    match[0].classList.remove('clic')
    match[0].classList.remove('clic')
}
/**
 * @param  {} {letdifferente=document.getElementsByClassName('clic'
 * @param  {} differente[0].disabled=falsedifferente[1].disabled=falsedifferente[0].textContent=''differente[1].textContent=''differente[0].classList.remove('clic'
 * @param  {} differente[0].classList.remove('clic'
 */
// Lorsque mes cartes sont différentes
function notSame(){
    let differente = document.getElementsByClassName('clic')
    differente[0].disabled = false
    differente[1].disabled = false
    differente[0].textContent = ''
    differente[1].textContent = ''
    differente[0].classList.remove('clic')
    differente[0].classList.remove('clic')			
}

/**
 * @param  {-} {jeu.style.display='none'timer.style.display='none'h1.innerHTML='Félicitation!Vousavezgagné!
 */
// Lorsque j'ai une victoire
function victoire(){
    jeu.style.display = 'none'
    timer.style.display = 'none'
    h1.innerHTML = 'Félicitation ! Vous avez gagné ! :-)'
    h2.innerHTML = ''
    id.innerHTML = 'Vous avez trouvé toutes les paires !'
}
/**
 * @param  {} {jeu.style.display='none'timer.style.display='none'h1.innerHTML='Lejeuestterminé
 * @param  {-('h2.innerHTML=''id.innerHTML="Vousn'avezpasréussiatrouvétouteslespaires"}} vousavezperdu...
 * @returns h2
 */
// Si la partie est terminé & que j'ai perdu
function perdu(){
    jeu.style.display = 'none'
    timer.style.display = 'none'
    h1.innerHTML = 'Le jeu est terminé, vous avez perdu... :-('
    h2.innerHTML = ''
    id.innerHTML = "Vous n'avez pas réussi a trouvé toutes les paires"
 }
